import sys
import traceback
from urllib.parse import urljoin, urlparse

from selenium import webdriver as wb
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

from src.requests_utils import extract_metadata_requests
from src.selenium_utils import extract_metadata_selenium
from src.utils import find_all_products, find_all_url, flatten


# Function that returns true if the website_link is a product page
def is_prod_page(website_link, selenium_driver, strategy="selenium"):
    urls = []
    prodotti = []
    if strategy == "selenium":
        metadata = extract_metadata_selenium(website_link, selenium_driver)
    else:
        metadata = extract_metadata_requests(website_link)
    find_all_products(metadata, "@type", "Product", prodotti)
    find_all_url(prodotti, "url", urls)
    cleaned_url = set(flatten(urls))
    print(cleaned_url)
    if urljoin(website_link, urlparse(website_link).path) in cleaned_url:
        print("Il link fornito è una product page !")
        return True
    else:
        print("Il link fornito non è una product page")
        return False


# Function that activates selenium driver
def get_driver():
    options = Options()
    options.add_argument("--headless")
    options.add_argument(
        "user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36")
    service = Service("C:\Program Files (x86)\chromedriver.exe")
    driver = wb.Chrome(service=service, options=options)
    driver.set_page_load_timeout(20)
    return driver


# Function that uses both selenium and requests
def metadata_prod_page(url_input, selenium_driver):
    try:
        sel = is_prod_page(url_input, selenium_driver, strategy="selenium")
        req = is_prod_page(url_input, selenium_driver, strategy="requests")
    except Exception:
        print("Errore metadata prod page")
        print(traceback.format_exc())
        driver.quit()
        sys.exit(1)
    if sel or req:
        return True
    else:
        return False


try:
    driver = get_driver()
except Exception:
    print("Errore da get_driver")
    print(traceback.format_exc())
    sys.exit(1)

if (len(sys.argv)) > 1:
    url = sys.argv[1]
else:
    url = ""

metadata_prod_page(url, driver)
driver.quit()
