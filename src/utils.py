# Function that extracts all the products in metadata -> saves products to var prodotti
def find_all_products(metadata, key, value, prodotti):
    if type(metadata) is dict:
        dict_parse_products(metadata, key, value, prodotti)
    elif type(metadata) is list:
        list_parse_products(metadata, key, value, prodotti)
    else:
        print("Impossibile effettuare il parse!")


def dict_parse_products(dictionary, key, value, prodotti):
    for item in dictionary:
        if item.casefold() == key.casefold() and value.casefold() in dictionary[item].casefold():
            prodotti.append(dictionary)
        if type(dictionary[item]) is dict:
            dict_parse_products(dictionary[item], key, value, prodotti)
        if type(dictionary[item]) is list:
            list_parse_products(dictionary[item], key, value, prodotti)


def list_parse_products(lista, key, value, prodotti):
    for item in lista:
        if type(item) is dict:
            dict_parse_products(item, key, value, prodotti)

        elif type(item) is list:
            list_parse_products(item, key, value, prodotti)


# Function for url extraction from products -> saves url to var urls
def find_all_url(metadata, key, urls):
    if type(metadata) is dict:
        dict_parse_url(metadata, key, urls)
    elif type(metadata) is list:
        list_parse_url(metadata, key, urls)
    else:
        print("Impossibile effettuare il parse!")


def dict_parse_url(dictionary, key, urls):
    for item in dictionary:
        if key.casefold() in item.casefold():
            urls.append(dictionary[item])
        if type(dictionary[item]) is dict:
            dict_parse_url(dictionary[item], key, urls)
        if type(dictionary[item]) is list:
            list_parse_url(dictionary[item], key, urls)


def list_parse_url(lista, key, urls):
    for item in lista:
        if type(item) is dict:
            dict_parse_url(item, key, urls)

        elif type(item) is list:
            list_parse_url(item, key, urls)


# Flattens a List
def flatten(ls):
    rt = []
    for i in ls:
        if isinstance(i, list):
            rt.extend(flatten(i))
        else:
            rt.append(i)
    return rt
