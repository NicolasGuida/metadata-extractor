import extruct
from w3lib.html import get_base_url


# Function for metadata extraction with Selenium
def extract_metadata_selenium(url, selenium_driver):
    selenium_driver.get(url)
    response = selenium_driver.page_source
    base_url = get_base_url(response, selenium_driver.current_url)
    metadata = extruct.extract(response,
                               base_url=base_url,
                               uniform=True,
                               syntaxes=['json-ld',
                                         'microdata',
                                         'opengraph',
                                         'rdfa'])
    return metadata
