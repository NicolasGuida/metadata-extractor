import extruct
import requests
from w3lib.html import get_base_url


# Function for metadata extraction with Requests
def extract_metadata_requests(url):
    response = requests.get(url, timeout=20)
    if response.ok:
        base_url = get_base_url(response.text, response.url)
        metadata = extruct.extract(response.text,
                                   base_url=base_url,
                                   uniform=True,
                                   syntaxes=['json-ld',
                                             'microdata',
                                             'opengraph',
                                             'rdfa'])
        return metadata
    else:
        return None
